/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2021, DarkElvenAngel
 * All rights reserved.
 * 
 * see LICENCE for full Licence
 */
// VT52 Digital Terminal Escape Codes
// Process Keyboard to send correct VT52 output
#include "pt52io.h"
#include <string.h>
#include <stdlib.h>

bool _Prase_ESC(char data)
{
    // char_buffer[Cursor.Line][Cursor.Column++] = (data + 0x100);
    // Cursor_Position_Check();
    static bool vt52_Y = false;
    static char More = 0;
    static uint8_t count = 0;
    static uint8_t params[2] = { 0 };
    if (data < 0x20 || data == 0x7F)                    // These are control characters and we want to display them
    {
        Put_char(data);
        return false;
    }
    if (More) { switch (More)
    {
        case 'Y' : // ESC Y [line] + 040 [column] + 040
            params[count++] = data - 32;
            if (count == 2) // COMPLETE
            {
                Move_cursorto(params[1],params[0]);
                count = 0;
                break;
            }
            return true;
        case 'b' : // ESC b [colour] + 32
            if (count == 0 && (data - 32) < 16)
            {
                Set_BColour(data - 32);
                break;
            }
            params[count] = data;
            if (count == 1)
            {
                Set_BColour(strtol(&params,NULL,16));
                count = 0;
                break;
            }
            count++;
            return true;// GET HEX COLOUR 
        case 'f' : // ESC f [colour] + 32
            if (count == 0 && (data - 32) < 16)
            {
                Set_FColour(data - 32);
                break;
            }
            params[count] = data;
            if (count == 1)
            {
                Set_FColour(strtol(&params,NULL,16));
                count = 0;
                break;
            }
            count++;
            return true;// GET HEX COLOUR 
        case 'g' :
            Use_FontSet((data - 32) % MAX_FONTSETS);
            break;
        case 'p' : // ESC f [colour] + 32
            Set_Pallet(data - 32);
            break;
        case '_' : // custom character 
            if (count == 0)
            {
                params[0] = data - FONTSET_START ; 
                if (params[0] >= FONTSET_SIZE) return false;
                Clear_User_Char(data);
                count++;
                return 1; 
            }
            //params[(count % 2)] = data & 0x0f;                              // Save low nibble of data shift if params[0] selected
            //if (count % 2 == 0)
            //{
            //char_data[((count - 1)  / 2) + 1] |= ((data & 0x0f) << ( 8 * count % 2));            // Save byte to character data
            // data &= 0xf;
            data = strtol(&data,NULL,16);
            //data <<= 4 * (count % 2);
            USR_FONT[(params[0] * 8) +  ((count - 1)  / 2) ] |= data << (4 * (count % 2));//data;
            //char_data[((count - 1)  / 2) + 1] |= ((data & 0x0f) << ( 4 * count % 2));
            //}
            if (count == 16 )
            {
                count = 0;
                break;
            }
            count++;
            return true;
        case '[':
            if ((data >= '0' && data <= ';') || data == '?') return 1;
            break;
    }
    More = 0;
    return false;
    }
    switch (data)
    {
    case 'A': Move_cursor(0,-1); break; // Up
    case 'B': Move_cursor(0, 1); break; // Down
    case 'C': Move_cursor(1, 1); break; // Right
    case 'D': Move_cursor(1,-1); break; // Left
    case 'T': 
        for (uint8_t L = 0; L < MAX_LINE; L++)
            for (uint8_t C = 0; C < MAX_COL; C++)
            {
#ifdef FULL_CHARBUFFER
                char_buffer[L][C].character = 'E';
#ifdef CHARBUFFER_SUPPORT_COLOUR_16
                char_buffer[L][C].colours = 0x0f;
#endif
#else
                char_buffer[L][C] = 'E';
#endif
            }
        break;
    case 'F': 
        Set_Attrib_Extended(true); 
        break;                           // Graphics Mode
    case 'G': 
        Set_Attrib_Extended(false);
        break;                          // Text Mode
    case 'g': More = data; return 1;    // Set Graphics Set
    case 'H': Home(0); break;           // Home
    case 'I': Reverse_Linefeed();break; // Reverse Linefeed
    case 'J': Clear_screen(0); break;   // Clear to end of screen
    case 'j': Clear_screen(2); break;   // Clear screen
    case 'K': Clear_line(0); break;     // Clear to end of line
    case 'k': Clear_line(2); break;     // Clear line
    case 'Y': More = data; return 1;    // Move to line column EXPECTS MORE INPUT
    case 'Z':                           // Report Terminal Type
        uart_puts(UART_ID, "\e/K");     // Respond ESC / K  VT52[No-Printer]
        break;
    case '5': Set_Cursor(true); break;  // Show Cursor
    case '6': Set_Cursor(false); break; // Hide Cursor
    case '7': Save_cursor(); break;     // Save Cursor
    case '8': Restore_cursor(); break;  // Restore Cursor
    case '=': break;                    // Alternet Keypad mode on
    case '>': break;                    // Alternet Keypad mode off
    case 'b':                           // Set Background Colour EXPECTS MORE INPUT
    case 'f': More = data; return 1;    // Set Foreground Colour EXPECTS MORE INPUT
    case 'p': More = data; return 1;    // Set pallet EXPECTS MORE INPUT
    case 'd': Set_FColour(7); Set_BColour(0);  // Reset Colour and Attributes
    case 'c': Set_Attrib_Flags(0); break;
    case 'S': Set_Attrib_Bold(1);break;
    case 's': Set_Attrib_Bold(0);break;
    case 'R': Set_Attrib_Reverse(1);break;    // Reverse on
    case 'r': Set_Attrib_Reverse(0);break;    // Reverse off
    case 'U': Set_Attrib_Underline(1);break;    // Underline on
    case 'u': Set_Attrib_Underline(0);break;    // Underline off
    case '_': More = data; return 1;            // Define custom character
    case '[': More = data; return 1;    // CSI mode.
    case 'N': Buffer_Flags.Cr_Lf = 1; break; 
    case 'n': Buffer_Flags.Cr_Lf = 0; break; 
    default:

        break;
    }
    return false;
}