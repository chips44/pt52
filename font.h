
#ifndef FONT_H
#define	FONT_H

#define byte   unsigned char

#define	GLYPH_WIDTH   8
#define	GLYPH_HEIGHT  8
#define MAX_FONTSETS  4
#define FONTSET_START 94
#define FONTSET_SIZE  33

// REGISTER FONTS
extern byte font_reg[0x100 * 8];
extern byte font_bold[0x100 * 8];
extern byte USR_FONT[FONTSET_SIZE * GLYPH_HEIGHT];

enum FONT_SETS {
    FS_BLOX,
    FS_VT52,
    FS_VT100,
    FS_BOX,
};

/**
 * Set User Defineable Character
 * 
 * \param character to replace
 * \param font_data 8 byte array font
 * \return none
 */
void Define_User_Char(char character, const byte font_data[8]);

/**
 * Select a pre-defined font set
 * 
 * \param fontset which set to load
 * \return none
 */
void Use_FontSet(enum FONT_SETS _fs);

/**
 * Clear User Defineable Character
 * 
 * \param character to delete
 * \return none
 */
void Clear_User_Char(char character);

#endif