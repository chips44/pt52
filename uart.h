#ifndef UART_H
#define UART_H
#include "hardware/irq.h" 

#define UART_ID uart1

#pragma pack(1)
struct uart_settings
{
    uint32_t baudrate;
    uint8_t  data_bits : 4;
    uint8_t  stop_bits : 2;
    uint8_t  parity    : 2;
};

struct uart_settings current_uart_setting;
struct uart_settings config_uart_setting;

void UART_init(irq_handler_t Uart_RX);
void UART_setup(int baud, uint8_t databits, uint8_t stopbits, uint8_t parity);
void UART_close();
#endif