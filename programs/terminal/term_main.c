#include <stdio.h>
#include <stdlib.h> // system()
#include <string.h> // memset()
#include <ctype.h>  // tolower()
#include "pt52io.h"



/**
 * Serial Data avaliable Event
 * 
 * return none
 */
void term_serial_in()
{
    while (uart_is_readable(UART_ID)) Decode_Character(uart_getc(UART_ID));
}
extern struct uart_settings config_uart_setting;
extern struct uart_settings current_uart_setting;
uint8_t term_menu()
{
    Set_Cursor(false);
    int baudrate[3] = { 115200, 2400, 9600 };
    int baudrate_index = 0;
    bool bit_count = 1;
    bool stop_bits = 0;
    UART_close();
    Reset_Character_Buffer();
    printf("\eb\044\eS\ef\057 Serial Terminal Menu\es\ef\047%80s\eb\040\r\n\n"," ");
    puts("\t\tSerial setting");
    Enter();
    printf ("\tb > Baud rate : %d\r\n", current_uart_setting.baudrate );
    printf ("\td > Data bits : %d\r\n", current_uart_setting.data_bits );
    printf ("\ts > Stop bits : %d\r\n", current_uart_setting.stop_bits );
    printf ("\tp > Parity    : %d\r\n", current_uart_setting.parity );
    printf ("\tr > Return to terminal\r\n");
    printf ("\tq > Quit to shell\r\n");
    bool in_menu = true;
    while (in_menu)
    {
        switch (tolower(getchar())) {
            case 'b': Move_cursorto(23,4); Clear_line(0); if (++baudrate_index > 2) baudrate_index = 0; printf("%d",baudrate[baudrate_index]);  break;
            case 'd': Move_cursorto(23,5); Clear_line(0); bit_count -= 1; printf("%d",7 + bit_count); break;
            case 's': Move_cursorto(23,6); Clear_line(0); stop_bits -= 1; printf("%d",1 + stop_bits); break;
            case 'p': Move_cursorto(23,7); Clear_line(0); puts("CHANGED"); break;
            case 'r': in_menu = false; break;
            case 'q': Home(0); Clear_screen(2); Set_Cursor(true); return false;
        }
    }
    Home(0);
    Clear_screen(2);
    UART_setup(baudrate[baudrate_index], 7 + bit_count, 1 + stop_bits, PARITY);
    UART_init(term_serial_in);
    puts("Terminal ONLINE");
    Set_Cursor(true);
    return true;
}

int app_term(int argc, char **argv)
{
    Use_FontSet(FS_VT52);
    bool not_quit = true;
#if 0
    Reset_Character_Buffer();
    printf("\eb\044\eS\ef\057 Serial Terminal\es\ef\047%80s\eb\040\r\n\n"," ");
    puts("Serial setting");
    Enter();
    printf ("> Baud rate : %d\r\n", BAUD_RATE );
    printf ("> Data bits : %d\r\n", DATA_BITS );
    printf ("> Stop bits : %d\r\n", STOP_BITS );
    printf ("> parity    : %d\r\n", PARITY );
    for(uint8_t c = 0; c <= 20; c++) char_buffer[4][c].colours = 0x040F;
    Enter();
    puts("press any key to start Terminal");
    getchar();
#endif
    if (current_uart_setting.baudrate == 0)
        UART_setup(BAUD_RATE, DATA_BITS, STOP_BITS, PARITY);
    else 
        UART_setup(current_uart_setting.baudrate, current_uart_setting.data_bits, current_uart_setting.stop_bits, current_uart_setting.parity);
    UART_init(term_serial_in);


    Reset_Character_Buffer();
    printf("\eR Serial Terminal%80s\er\r\n"," ");
    puts ("Terminal ONLINE");
    while (not_quit)
    {
        keycode_t key = get_lastkey();
        switch (key.character)
        {
                case 0: switch (key.keycode) // non-printable characters
                {
                    case 0x1F: if (key.ctrl) uart_putc_raw(UART_ID, 0);break;   // CTRL @ send NULL
                    case 0x46: Reset_Character_Buffer(); break;             // PRINT SCREEN
                    case 0x48: if (key.ctrl) uart_puts(UART_ID, "PT52\n");
                    case 0x4c: uart_putc_raw(UART_ID, 127);break;           // Delete key
                    case 0x4f: uart_puts(UART_ID, "\eC"); break;            // cursorX += cursorX <= COLS-1 ? 1 : 0; break;// right
                    case 0x50: uart_puts(UART_ID, "\eD"); break;            // cursorX -= cursorX > 0 ? 1 : 0; break;// left
                    case 0x51: uart_puts(UART_ID, "\eB"); break;            // cursorY += cursorY <= FOOTER - 2 ? 1 : 0; break;// down
                    case 0x52: uart_puts(UART_ID, "\eA"); break;            // cursorY -= cursorY > HEADER ? 1 : 0; break;//up
                    case 0x65: not_quit = term_menu(); break;
                    default  : uart_putc_raw(UART_ID, key.character);break;
                } 
                break; 
                //case 0x08 : uart_putc_raw(UART_ID, ch);break;             // BackSpace 
                //case 0x09 : uart_putc_raw(UART_ID, ch);break;             // Tab Key
                //case 0x1b : uart_putc_raw(UART_ID, ch);break;             // Escape key
                //case '\r' : uart_puts(UART_ID, "\n\r");break;             // ENTER []
                default   :  uart_putc_raw(UART_ID, key.character);break;
        }
    }
    return 0;
}