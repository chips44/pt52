#include <stdio.h>
#include <stdlib.h> // system()
#include <string.h> // memset()
#include <ctype.h>  // tolower()
#include "bsp/board.h" // board_millis()
// #include "my_basic.h" no good!
#include "ubasic.h"
#include "tokenizer.h"
#include "pt52io.h"

//extern int my_basic_shell();
#define _str_eq(__str1, __str2) (strcasecmp((__str1), (__str2)) == 0)


int app_basic()
{
    char *Program = malloc(1024 * 8); // Allocate 8k of program memory
    if (Program == 0)
    {
        printf("Insficent Memory");
        return 1;
    }
    bool run_basic = true;
    Reset_Character_Buffer();
    printf("\eb\044\eS\ef\057 UBASIC (pico edition)\es\ef\047%80s\eb\040\r\n\n"," ");
    while (run_basic)
    {
        char *line;
        line = readline(NULL);
        if(_str_eq(line, "")) {
		/* Do nothing */
        //} else if(_str_eq(line, "HELP")) {
        //    printf("OK\n\r");
        } else if(_str_eq(line, "CLS")) {
            Home(0);
            Clear_screen(0);
            printf("OK\n\r");
        } else if(_str_eq(line, "NEW")) {
            memset(Program,0,1024 * 8);
            printf("OK\n\r");

        } else if(_str_eq(line, "RUN")) {
            ubasic_init(Program);
            do {
                ubasic_run();
            } while(!ubasic_finished());
            printf("OK\n\r");
            
        } else if(_str_eq(line, "BYE")) {
            printf("EXITING BASIC\n\r");
            run_basic = false;
        } else if(_str_eq(line, "LIST")) {
            printf("%s",Program);
        } else if(_str_eq(line, "EDIT")) {
            printf("OK\n\r");
        } else if(isdigit(*line)){
            strcat(Program,line);
            strcat(Program,"\n\r");
        } else {
            char *runline = 0;
            runline = malloc(162);
            if (runline)
            {
                runline [0] = '0';
                runline [1] = ' ';
                strcpy(&runline[2], line);
                ubasic_init(runline);
                do {
                    ubasic_run();
                } while(!ubasic_finished());
                free(runline);
            } else {
                printf("MEMORY ERROR\n\r");
            }
            //printf("SYNTAX ERROR\n\r");
            //
        }
        free(line);
    }
    free(Program);
    return 0;
}