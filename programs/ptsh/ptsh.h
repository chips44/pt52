#ifndef PTSH_H
#define PTSH_H
#define PTSH_VERSION "0.0.1"

#include <setjmp.h>

extern jmp_buf ptsh_buffer;
void PTSH_EXIT(int);
void ptsh_loop(void);

#undef exit
#define exit(e) PTSH_EXIT(e)

#endif