/*
The MIT License (MIT)

Copyright (c) 2014 Ivaylo Stoyanov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/// This source code has been modified to expand capabilities and run on the PT52

#include "pico.h"
#include <stdio.h>
#include <math.h>   // ceil()
#include <string.h> // memset()
#include <ctype.h>  // isprint()
#include "hex_functions.h"
#include "pt52io.h"

// Default console window size is 80x25
#define CONSOLE_COLS  80 // console width (characters)
#define CONSOLE_LINES 24 // console height (lines)

#define HEX_LINES 19 // lines of hex per page
#define SPACER    15 // space between hex and ASCII (up to 15)

unsigned getPageCount(uint32_t address/*FILE *pSrc*/) {
    long int fileSize;
    const int limit = address != 0 ? 16777216 : 16384;
    // fseek(pSrc, 0, SEEK_END);
    // fileSize = ftell(pSrc);
    // rewind(pSrc);

    // Page count is equal to the file size divided by the size of the page
    return (unsigned) ceil(limit / (double)(HEX_LINES * 16));
}
unsigned getFilePageCount(FILE *pSrc) {
  long int fileSize;

  fseek(pSrc, 0, SEEK_END);
  fileSize = ftell(pSrc);
  rewind(pSrc);

  // Page count is equal to the file size divided by the size of the page
  return (unsigned) ceil(fileSize / (double)(HEX_LINES * 16));
}


void printHex (uint32_t address,unsigned long page)
{
    uint8_t *pSrc;
    const int limit = address != 0 ? 16777216 : 16384;
    int c;
    unsigned hexIndex = 0, printablesIndex = 0, readBytes = 0;
    char currentLine[CONSOLE_COLS];
    // Move the file indicator to the page provided
    //fseek(pSrc, page * HEX_LINES * 16, 0);
    pSrc = (uint8_t*)(address + (page * HEX_LINES * 16));
    memset(currentLine, ' ', CONSOLE_COLS); // Fills the array with spaces
    currentLine[CONSOLE_COLS - 1] = '\0'; // Inserts a null-terminator
    Save_cursor();
    Move_cursorto (60,0);
    printf("\eR0x%08X\er", pSrc);
    Restore_cursor();
    Set_FColour(10);
    for (;;) {
        c = (uint8_t)(*pSrc);pSrc++;
        ++readBytes;

        // Put anything but EOF NO PRINT EVERYTHING!!!! MUhahahahaha
        //if (c != EOF) {
        // Put hex code
        sprintf(currentLine + hexIndex, " %02X", c);
        currentLine[hexIndex + 3] = ' '; // removes the \0, inserted by sprintf() after the hex byte

        // Put printable characters in the right hand part of the array
        if (isprint(c))
            currentLine[48 + SPACER + printablesIndex++] = c;   // 48 = 16 hex bytes * 3 chars wide each
        else
            currentLine[48 + SPACER + printablesIndex++] = '.';

        hexIndex += 3; // increment by 3 since a hex byte is 3 chars wide (including 1 space)
        //}

        // If 16 bytes are read or EOF is reached
        if (printablesIndex == 16/* || c == EOF*/) {
            Home(1);
            puts(currentLine); // Print the line

            /*if (c == EOF)
                break;
                */
            // Reset the variables
            hexIndex = 0;
            printablesIndex = 0;
            memset(currentLine, ' ', CONSOLE_COLS);
            currentLine[CONSOLE_COLS - 1] = '\0';
        }
        if ( (int)pSrc == (address + limit) ) break;
        // Stop reading after the page is full
        if (readBytes == HEX_LINES * 16) break;
    }
    Set_FColour(7);
}

void printFileHex(FILE *pSrc, unsigned long page) {
    int c;
    unsigned hexIndex = 0, printablesIndex = 0, readBytes = 0;
    char currentLine[CONSOLE_COLS];

    // Move the file indicator to the page provided
    fseek(pSrc, page * HEX_LINES * 16, 0);

    memset(currentLine, ' ', CONSOLE_COLS); // Fills the array with spaces
    currentLine[CONSOLE_COLS - 1] = '\0'; // Inserts a null-terminator
    
    Set_FColour(10);
    for (;;) {
        c = getc(pSrc);
        ++readBytes;

        // Put anything but EOF
        if (c != EOF) {
            // Put hex code
            sprintf(currentLine + hexIndex, " %02x", c);
            currentLine[hexIndex + 3] = ' '; // removes the \0, inserted by sprintf() after the hex byte

            // Put printable characters in the right hand part of the array
            if (isprint(c))
                currentLine[48 + SPACER + printablesIndex++] = c;   // 48 = 16 hex bytes * 3 chars wide each
            else
                currentLine[48 + SPACER + printablesIndex++] = '.';

            hexIndex += 3; // increment by 3 since a hex byte is 3 chars wide (including 1 space)
        }

        // If 16 bytes are read or EOF is reached
        if (printablesIndex == 16 || c == EOF) {
            Home(1);
            puts(currentLine); // Print the line

            if (c == EOF)
                break;

            // Reset the variables
            hexIndex = 0;
            printablesIndex = 0;
            memset(currentLine, ' ', CONSOLE_COLS);
            currentLine[CONSOLE_COLS - 1] = '\0';
        }

        // Stop reading after the page is full
        if (readBytes == HEX_LINES * 16)
        break;
    }
    Set_FColour(7);
}

void hex_menu(uint32_t* basemem)
{
    /*
        0x0000_0000   16kb    0       boot rom
        0x1000_0000   16mb    8       XIP flash cacheable, allocating, normal
        0x1100_0000   16mb    8       XIP flash cacheable, non-allocating
        0x1200_0000   16mb    8       XIP flash non-cacheable, allocating
        0x1300_0000   16mb    8       XIP flash non-cachecable, non-callocating
        0x1400_0000                   XIP control registers
        0x1500_0000   16kb            XIP cache, only accessible if cache disabled
        0x1800_0000                   SSI
        0x2000_0000   256kb   1/2/3/4 raid0 over sram[0,1,2,3], 16 byte stripes, each 32bit int within a stripe goes to a different ram bank
        0x2004_0000   4kb     5       sram
        0x2004_1000   4kb     6       sram
        0x2100_0000   64kb    1       sram0
        0x2101_0000   64kb    2       sram1
        0x2102_0000   64kb    3       sram2
        0x2103_0000   64kb    4       sram3
        0x4000_0000           7       APB Bridge
        0x5000_0000           9       AHB-Lite Splitter
    */
    puts(" SELECT MEMORY REGION TO VIEW ");
    puts(" 0 )\tBOOT ROM");
    puts(" 1 )\tXIP flash");
    puts(" 2 )\tXIP flash");
    puts(" 3 )\tXIP flash");
    puts(" 4 )\tsram");
    
    switch (tolower(getchar())) {
        case '0': *basemem = 0; break;
        case '1': *basemem = 0x10000000; break;
        case '2': *basemem = 0x11000000; break;
        case '3': *basemem = 0x13000000; break;
        case '4': *basemem = 0x20000000; break;
    }


}
